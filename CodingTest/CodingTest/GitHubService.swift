//
//  GitHubService.swift
//  CodingTest
//
//  Created by Antony on 2022/03/27.
//

import Foundation

struct GitHubService: Service {
    public static let shared = GitHubService()
    private init() {}
    
    func fetchGitHubRepos(_ parameter: RequestParameter, completion: @escaping ((Result<GitHubRepo, Error>) -> Void)) {
        guard let request = parameter.request else {
            completion(.failure(APIError.customError("request is nil")))
            return
        }
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForResource = 5
        config.httpAdditionalHeaders = ["Accept": "application/vnd.github.v3+json"]
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(APIError.serviceError(error)))
                    return
                }
                
                if let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode != 200 {
                    completion(.failure(APIError.customError("Wrong response code: \(statusCode)")))
                    return
                }
                
                guard let data = data else {
                    completion(.failure(APIError.customError("No feedback data")))
                    return
                }
                
                do {
                    let repos = try JSONDecoder().decode(GitHubRepo.self, from: data)
                    
                    completion(.success(repos))
                } catch {
                    completion(.failure(APIError.customError("Decoding failede")))
                }
                
            }
        }
        task.resume()
    }
}
