//
//  RequestParameter.swift
//  CodingTest
//
//  Created by Antony on 2022/03/27.
//

import Foundation

struct RequestParameter: Codable {
    var query: String
    var reposPerPage: String? // default: 30
    var page: String? // default: 1
    
    enum CodingKeys: String, CodingKey {
        case query = "q"
        case reposPerPage = "per_page"
        case page
    }
    
    // MARK: -
    
    var request: URLRequest? {
        var components = URLComponents()
        components.scheme = Api.scheme
        components.host = Api.host
        components.path = Api.Router.getRepositories
        components.queryItems = queryParams.map { URLQueryItem(name: $0, value: $1 as? String) }
        let url = components.url
        
        guard let url = url else {
            return nil
        }
        
        return URLRequest(url: url)
    }
    
    private var queryParams: [String: Any] {
        guard let data = try? JSONEncoder().encode(self) else {
            return [:]
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            return [:]
        }
        return json
    }
}
