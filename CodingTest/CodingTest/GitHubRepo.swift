//
//  GitHubRepo.swift
//  CodingTest
//
//  Created by Antony on 2022/03/27.
//

import Foundation

struct GitHubRepo: Codable {
    let totalCount: Int
    var items: [Repo]
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case items = "items"
    }
    
    var itemCount: Int {
        items.count
    }
}

struct Repo: Codable {
    let fullName: String
    let htmlURL: String
    
    enum CodingKeys: String, CodingKey {
        case fullName = "full_name"
        case htmlURL = "html_url"
    }
}
