//
//  Constants.swift
//  CodingTest
//
//  Created by Antony on 2022/03/27.
//

import Foundation

enum Api {
    static let scheme = "https"
    static let host = "api.github.com"
    
    enum Router {
        static let getRepositories = "/search/repositories"
    }
}
