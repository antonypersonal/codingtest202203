//
//  ViewController.swift
//  CodingTest
//
//  Created by Antony on 2022/03/27.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private var pendingRequestWorkItem: DispatchWorkItem?
    
    private var repos = [Repo]()
    
    private var currentSearchText: String?
    private var totalItems = 0
    private var currentPage = 1
    private var isFetching = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        currentSearchText = searchText
        currentPage = 1
        repos.removeAll()
        
        if searchBar.text == "" || searchBar.text == nil {
            tableView.reloadData()
            return
        }
        
        pendingRequestWorkItem?.cancel()

        let requestWorkItem = DispatchWorkItem { [weak self] in
            self?.fetchGitHubRepos(searchText: searchText)
        }

        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100), execute: requestWorkItem) // Call the API every 100 milliseconds
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        currentPage = 1
        if let text = searchBar.text, text != ""  {
            fetchGitHubRepos(searchText: text)
        }
        searchBar.resignFirstResponder()
    }
    
    private func fetchGitHubRepos(searchText: String) {
        guard isFetching == false else {
            return
        }
        
        isFetching = true
        
        GitHubService.shared.fetchGitHubRepos(RequestParameter(query: searchText, page: "\(self.currentPage)")) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case let .success(repo):
                self.isFetching = false
                self.totalItems = repo.totalCount
                self.currentPage += 1
                
                self.repos.append(contentsOf: repo.items)
                
                self.tableView.reloadData()
                
            case let .failure(error):
                self.isFetching = false
                if let error = error as? APIError {
                    switch error {
                    case .serviceError(let error):
                        self.showAlert(error.localizedDescription)
                        
                    case .customError(let string):
                        self.showAlert(string)
                    }
                }
            }
        }
    }
    
    private func showAlert(_ message: String) {
        let alert = UIAlertController(title: "NOTE",
                                      message: message,
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true)
    }
}

// MARK: - UITableViewDataSource

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        repos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell", for: indexPath)
        
        let repo = repos[indexPath.row]
        cell.textLabel?.text = repo.fullName
        return cell
    }
}

// MARK: - UITableViewDataSourcePrefetching

extension ViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            let needFetch = indexPath.row == repos.count - 8
            if let currentSearchText = currentSearchText, currentSearchText != "", needFetch {
                fetchGitHubRepos(searchText: currentSearchText)
            }
        }
    }
}

