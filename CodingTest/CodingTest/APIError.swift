//
//  APIError.swift
//  CodingTest
//
//  Created by Antony on 2022/03/27.
//

import Foundation

enum APIError: Error {
    case serviceError(Error)
    case customError(String)
}
