//
//  Service.swift
//  CodingTest
//
//  Created by Antony on 2022/03/27.
//

import Foundation

protocol Service {
    func fetchGitHubRepos(_ parameter: RequestParameter, completion: @escaping ((Result<GitHubRepo, Error>) -> Void))
}
